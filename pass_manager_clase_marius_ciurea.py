import os
import rsa

class PassManager:
    def __init__(self, file,  app='', password=''):
        self.file = file
        self.app = app
        self.password = password
        if os.path.exists(self.file) == False:   # daca nu exista fisierul dat ca parametru in constructor sa il creeze
            with open(self.file, 'w') as f:      # deschiderea fisierului pentru scriere
                pass
    @staticmethod
    def menu():
        option = ''
        while option != '1' and option != '2' and option != '3':
            option = input('1.Salveaza o parola\n'
                           '2.Alege aplicatia pentru care sa vezi parola\n'
                           '3.Vezi toate parolele\n'
                           'Alege o optiune:')
        return option

    def input_app(self):
        self.app = input('Introduceti aplicatia:')

    def input_password(self):
        self.password = input('Introduceti parola:')


    def read_from_file(self, mode, whole=False):
        if whole == False:
            with open(self.file, mode) as fr:
                file_content = fr.readlines()
                return file_content

        with open(self.file, mode) as fr:
            file_content = fr.read()
            return file_content

    def write_to_file(self, mode, content):
        with open(self.file, mode) as fw:
            fw.write(content)


    def save_data_to_file(self):
        file_content = self.read_from_file('r', whole=True)  # whole= True - ca sa salveza ca string nu ca lista, ramura 2 read_from_file
        if self.app in file_content:
            print('aplicatia exista in fisier')
        else:
            self.write_to_file('a', f'{self.app}: {self.password}\n')   # 'a' vine de la append - adica se scrie in fisier

    def encrypt_file(self):      # presupune existenta unei chei publice
        pubkey, privkey = rsa.newkeys(1024)
        with open(f'../public.pem', 'wb') as fw:
            fw.write(pubkey.save_pkcs1('PEM'))
        with open(f'../keys/private.pem', 'wb') as fw:
            fw.write(privkey.save_pkcs1('PEM'))
        content = self.read_from_file('r', whole=True)
        enc_content = rsa.encrypt(content.encode('utf-8'), pubkey) # aici s-a facut criptarea
        self.write_to_file('wb', enc_content)


    def decrypt_file(self):      # presupune existenta unei chei private
        if os.path.exists(self.file) and os.path.getsize(self.file) > 0:
            with open(f'../keys/private.pem', 'rb') as fr:
               priv_key = rsa.PrivateKey.load_pkcs1(fr.read())
            content = self.read_from_file('rb', whole=True)
            decrypted_content = rsa.decrypt(content, priv_key) # aici s-a facut decriptarea

            self.write_to_file('w', decrypted_content.decode('utf-8') )

pm = PassManager('password_manager_file.txt')

pm.decrypt_file()
option = ''

while option.upper() != 'Y':
    option = pm.menu()
    if option == '1':
        pm.input_app()
        pm.input_password()
        pm.save_data_to_file()   # dupa ce le introducem de la tastatura trebuie sa le salvam
    elif option == '2':
        pm.input_app()
        file_content = pm.read_from_file('r', whole=False) # se doreste ca sa se salveze continutul intr-o lista prin care sa iteram apoi
        print('*' * 33)
        for line in file_content:
            if pm.app in line:
                print(line.strip())
        print('*' * 33)
    elif option == '3':
        print('*' * 33)
        file_content = pm.read_from_file('r', whole=True)  # se doreste ca sa se salveze continutul intr-un string
        print(file_content.strip())
        print('*' * 33)

    option = input('Vrei sa iesi? [Y/N]')
    if option.upper() == 'Y':
        pm.encrypt_file()

# imbunatatiri:
# - informatiile pot fi scrise intr-o baza de date
# - poate fi creata si o interfata grafica
# - poate fi creata si o aplicatie web based